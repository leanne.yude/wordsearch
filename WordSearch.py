#  File: WordSearch.py

#  Description:

#  Student Name: Leanne Yude

#  Student UT EID: lcy239

#  Partner Name: Alan Tang

#  Partner UT EID: apt777

#  Course Name: CS 313E 

#  Unique Number: 52595; 52590

#  Date Created: 8/28/2021

#  Date Last Modified: 9/5/2021

import sys 

# Input: None
# Output: function returns a 2-D list that is the grid of letters and
#         1-D list of words to search
def read_input ( ):

    #read_input
    #read the first line 
    num_lines = int(sys.stdin.readline())
        
    #read blank line
    sys.stdin.readline()

    #read the grid and returns the coordination

    #empty list
    read_input_word_grid = []

    #for each row
    for row in range(num_lines):

        #read the first line of grid 
        line = sys.stdin.readline()

        #take out the spaces 
        line = line.replace(" ", "")
        
        #create empty list for each row 
        row_list = []

        #for each column
        for column in range(num_lines):
            
            #variable for each element  
            element = line[column]

            #add each element to the rowList 
            row_list.append(element)
        
        #add each rowList to the grid list 
        read_input_word_grid.append(row_list)
            
    #read blank line after grid 
    sys.stdin.readline()

    #read number of words listed; change str to int
    num_words = int(sys.stdin.readline())

    #create list for word list
    read_input_word_list = []

    #put each word into a 1D list 
    for words in range(num_words):
        
        #read the first word
        search_word = sys.stdin.readline() 

        #strips \n after each word
        search_word = search_word.rstrip('\n')

        #adds each word to word list
        read_input_word_list.append(search_word)

    #returns the 2D grid
    return read_input_word_grid, read_input_word_list


# Input: a 2-D list representing the grid of letters and a single
#        string representing the word to search
# Output: returns a tuple (i, j) containing the row number and the
#         column number of the word that you are searching 
#         or (0, 0) if the word does not exist in the grid
def find_word (grid, search_word):
  
    #create fake_string variable
    fake_string = ''

    #creates list for coordinates of first letter 
    first_letter_coordinates = []

    #loops through each row 
    for row in range(len(grid)):

        #if word found, break loop
        if fake_string == search_word:
            break
        
        #loops through each column
        for col in range(len(grid[0])):

            #variable k 
            k = 0 

            #special cases where the length of the word is only 1 
            if len(search_word) == 1 and grid[row][col] == search_word:
                
                #variable that equals row and column to prevent change of variable row and col 
                current_row = row
                current_col = col

                c_row = current_row 
                c_col = current_col

                #appends letter to fake string
                letter = search_word[k]
                fake_string += letter

                #adds 1 to coordinate
                first_letter_coordinates.append(c_row + 1)
                first_letter_coordinates.append(c_col + 1)

            #if word found, break loop
            if fake_string == search_word:
                break
                

            elif search_word[k] == grid[row][col] and fake_string != search_word:
    
                #Horizontal Right 
                    #if NOT at the right grid edge
                    #if spaces to the right is greater than or equal to length of word 
                if col != len(grid[0]) - 1 and fake_string != search_word and (len(grid[0])- col) >= len(search_word):
                    
                    #variable that equals row and column to prevent change of variable row and col
                    current_row = row
                    current_col = col

                    for i in range(len(search_word)):

                    
                        #checks to see if the rest of the letters match
                        if search_word[i] == grid [current_row][current_col] :

                            #add a column to the right 
                            current_col +=1

                            #we add the letter to a string 
                            letter = search_word[i]
                            fake_string += letter 

                        else: 
                            #if the next letter doesn't match then we remove 
                                #all the letters from the fake_string 
                            fake_string = ''
                            break
                    
                    if fake_string == search_word:
                        
                        #expressions that find coordinate of first letter if word found
                        c_row = current_row 
                        c_col = current_col - len(search_word)

                        #adds 1 to coordinate and appends it 
                        first_letter_coordinates.append(c_row + 1)
                        first_letter_coordinates.append(c_col + 1)
                       

                #Horizontal Left
                    #if the col+1 is greater or equal to len(search_word), then you can check for horizontal left
                    #ensures enough space to the left for the whole search_word or don't bother checking 
                    #if col is not at the left edge (0) and if we have not found the search_word yet 
                if col+1 >= len(search_word) and col != 0 and fake_string != search_word : 

                    #variable that equals row and column to prevent change of variable row and col
                    current_row = row
                    current_col = col

                    for i in range(len(search_word)):

                        #check to see if the rest of the letters match 
                        if search_word[i] == grid[current_row][current_col] and col != 0:
                            
                            #move column to left one 
                            current_col -= 1

                            #append letter to list 
                            letter = search_word[i]
                            fake_string += letter
                            

                        else:
                            #if the next letter doesn't match then we remove 
                                #all the letters from the fake_string 
                            fake_string = ''
                            break
                        
                        

                    if fake_string == search_word:

                        #expression to find coordinate of first letter if word found 
                        c_row = current_row 
                        c_col = current_col + len(search_word)

                        #adds 1 to coordinate and appends to list 
                        first_letter_coordinates.append(c_row + 1)
                        first_letter_coordinates.append(c_col + 1)
                     
                

                #Vertical down 
                    #if the row+1 is less than or equal to len(search_word), then you can check for vertical down
                    #ensures there is enough space for the whole search_word if you go down vertically 
                if (len(grid) - row) >= len(search_word) and fake_string != search_word and row != len(grid)-1 :
                    
                    #variable that equals row and column to prevent change of variable row and col
                    current_row = row
                    current_col = col
                   
                    for i in range(len(search_word)):

                        if search_word[i] == grid[current_row][current_col]:

                            #move one row down 
                            current_row += 1
                            
                            #appends letter to list 
                            letter = search_word[i]
                            fake_string += letter
                  
                        else:
                            #if the next letter doesn't match then we remove 
                                        #all the letters from the fake_string 
                            fake_string = ''
                           
                            break
                        
                    if fake_string == search_word:
                        
                        c_row = current_row - len(search_word)
                        c_col = current_col 

                        #adds 1 to coordinate
                        first_letter_coordinates.append(c_row + 1)
                        first_letter_coordinates.append(c_col + 1)

                        
                
                #Vertical up 
                    #should check for vertical up if 
                    #row + 1 greater than or equal to length of word; shows there is enough space for the word
                    #fake string not found 
                if row + 1 >= len(search_word) and fake_string != search_word and row != 0 :

                    #variable that equals row and column to prevent change of variable row and col
                    current_row = row
                    current_col = col
  
                    for i in range(len(search_word)): 

                        if search_word[i] == grid[current_row][current_col]:

                            #move one row up 
                            current_row -= 1

                            letter = search_word[i]
                            fake_string += letter 
                            

                        else: 
                            #if the next letter doesn't match then we remove 
                                #all the letters from the fake_string 
                            fake_string = ''
                            break

                    if fake_string == search_word:
                        
                        #expression that finds coordinate of first letter if word found
                        c_row = current_row + len(search_word)
                        c_col = current_col 

                        #adds 1 to coordinate
                        first_letter_coordinates.append(c_row + 1)
                        first_letter_coordinates.append(c_col + 1)

                        
                
                #Right diagonal 
                    #should check if word not found and if column is not at the right edge of the grid 
                if col != len(grid[0]) - 1 and fake_string != search_word : 

                    #Right UP Diagonal 
                        #if horizontally right and vertically up has greater than or equal to length of word 
                    if row != 0 and row + 1 >= len(search_word) and fake_string != search_word: 

                        #variable that equals row and column to prevent change of variable row and col
                        current_row = row
                        current_col = col

                        for i in range(len(search_word)): 

                            
                            if search_word[i] == grid[current_row][current_col] and (len(grid[0])-(col-i)) >= len(search_word) and fake_string != search_word:

                                ##move one col to right and one row up 
                                current_col += 1
                                current_row -= 1

                                letter = search_word[i]
                                fake_string += letter 

                            
                            else: 
                                #clear fake_string
                                fake_string = ''
                                break

                        if fake_string == search_word: 
                           
                            c_row = current_row + len(search_word) 
                            c_col = current_col - len(search_word) 

                            #adds 1 to coordinate and appends
                            first_letter_coordinates.append(c_row + 1)
                            first_letter_coordinates.append(c_col + 1)
            
                    #Right DOWN Diagonal
                        #if word not found
                        #if enough space to the right horizontal and down vertical is equal or greater than word length 
                    if (len(grid) - row) >= len(search_word) and (len(grid[0])- col) >= len(search_word) and row != len(grid)-1 and fake_string != search_word:

                        #variable that equals row and column to prevent change of variable row and col
                        current_row = row
                        current_col = col

                        #loops for amt of word length 
                        for i in range(len(search_word)): 

                            #if letter is equal to current grid letter and word still not found 
                            if search_word[i] == grid[current_row][current_col] and fake_string != search_word:

                                #adds one column up and one row up after each loop 
                                current_col += 1 
                                current_row += 1

                                #variable that equals row and column to prevent change of variable row and col
                                letter = search_word[i]
                                fake_string += letter 

                            else:

                                #clear fake_string
                                fake_string = ''
                                break
                        
                        if fake_string == search_word: 
                            
                            #subtracts from row and col for coordinates of first letter 
                            c_row = current_row - len(search_word) 
                            c_col = current_col - len(search_word) 

                            #adds 1 to coordinate
                            first_letter_coordinates.append(c_row + 1)
                            first_letter_coordinates.append(c_col + 1)
                           
                #Left Diagonals
                    #only check if col is not at the left edge/0   
                if col != 0 and fake_string != search_word:

                    #Left UP Diagonal 
                        #if vertically up and horizontally left is equal or greater than word length
                    if row + 1 >= len(search_word) and col+1 >= len(search_word) and row != 0 and fake_string != search_word: 

                        #variable that equals row and column to prevent change of variable row and col
                        current_row = row
                        current_col = col

                        for i in range(len(search_word)): 

                            #if letter matches current grid letter and word not found
                            if search_word[i] == grid[current_row][current_col] and fake_string != search_word:

                                #one row down and one column down after each loop 
                                current_row -= 1
                                current_col -= 1

                                #append letter to string
                                letter = search_word[i]
                                fake_string += letter 

                            else:

                                #clear fake_string
                                fake_string = ''
                
                                break
                        
                        #if word found
                        if fake_string == search_word: 
                            
                            #expression to find coordinate of first letter
                            c_row = current_row + len(search_word) 
                            c_col = current_col + len(search_word) 

                            #adds 1 to coordinate and appends to list 
                            first_letter_coordinates.append(c_row + 1)
                            first_letter_coordinates.append(c_col + 1)
                          
                
        
                    #Left DOWN Diagonal 
                        #conditions to even check for left down diagonal; 
                        #conditions same as left horizontal and vertical down 
                    if row != len(grid)-1 and col+1 >= len(search_word) and (len(grid) - row) >= len(search_word) and fake_string != search_word:

                        current_row = row
                        current_col = col

                        for i in range(len(search_word)):

                            #if search_word matches
                            if search_word[i] == grid[current_row][current_col] and fake_string != search_word:

                                #one col to the left and one row up 
                                current_col -= 1 
                                current_row += 1 

                                #append letter to string
                                letter = search_word[i]
                                fake_string += letter 

                            
                            else:
                                #clear fake_string
                                fake_string = ''
                                
                                break
                        
                        if fake_string == search_word: 
                            
                            #expression to find coordinate of first letter 
                            c_row = current_row - len(search_word) 
                            c_col = current_col + len(search_word) 

                            #adds 1 to coordinate and appends to list 
                            first_letter_coordinates.append(c_row + 1)
                            first_letter_coordinates.append(c_col + 1)
                    
    #if word not found then the coordinates should be 0 0                   
    if fake_string != search_word:
        first_letter_coordinates.append(0)
        first_letter_coordinates.append(0)

    #turn list into a tuple
    location = tuple(first_letter_coordinates)

    #returns tuple
    return location

def main():
  # read the input file from stdin
  word_grid, word_list = read_input()
  

  # find each word and print its location
  for word in word_list:
    location = find_word (word_grid, word)
    print (word + ": " + str(location))

if __name__ == "__main__":
  main()